using System.Linq;
using System.Threading.Tasks;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Repositories
{
    public class PersonRepository : BaseRepository<Person>
    {
        private readonly DataContext _context;
        
        public PersonRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Person> FindByLoginAndPassword(string login, string password)
        {
            return await _context.Persons.FirstOrDefaultAsync(x => x.Login == login && x.Password == password);
        }

        public async Task<Person> FindByLogin(string login)
        {
            return await _context.Persons.FirstOrDefaultAsync(x => x.Login == login);
        }
    }
}