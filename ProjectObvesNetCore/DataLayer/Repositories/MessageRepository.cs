using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Repositories
{
    public class MessageRepository : BaseRepository<Message>
    {
        private readonly DataContext _context;

        public MessageRepository(DataContext context) : base(context)
        {
            _context = context;
        }
        
        public async Task<List<Message>> GetAllAsync()
        {
            return await _context.Messages.ToListAsync();
        }
    }
}