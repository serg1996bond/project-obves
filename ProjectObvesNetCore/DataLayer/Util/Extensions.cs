using DataLayer.Entities;
using DataLayer.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace DataLayer.Util
{
    public static class Extensions
    {
        public static void AddDataLayer(this IServiceCollection services)
        {
            //репозитории
            services.AddScoped<BaseRepository<Message>,MessageRepository>();
            services.AddScoped<MessageRepository>();
            services.AddScoped<BaseRepository<Person>,PersonRepository>();
            services.AddScoped<PersonRepository>();
            //DB контекст
            services.AddDbContext<DataContext>();
        }
    }
}