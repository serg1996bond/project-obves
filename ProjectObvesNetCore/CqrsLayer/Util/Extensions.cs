using System.Collections.Generic;
using CqrsLayer.CommandHandlers;
using CqrsLayer.Commands;
using CqrsLayer.Dispatchers;
using CqrsLayer.Interfaces;
using CqrsLayer.Queries;
using CqrsLayer.QueryHandlers;
using Microsoft.Extensions.DependencyInjection;
using PresentationLayer.Dto.Message;

namespace CqrsLayer.Util
{
    public static class Extensions
    {
        public static void AddCqrsLayer(this IServiceCollection services)
        {
            //обработчики комманд
            services.AddScoped<ICommandHandler<CreateMessage>,CreateMessageHandler>();
            services.AddScoped<ICommandHandler<RemoveMessage>,RemoveMessageHandler>();
            services.AddScoped<ICommandHandler<UpdateMessage>, UpdateMessageHandler>();
            //обработчики запросов
            services.AddScoped<IQueryHandler<GetAllMessages, List<MessageViewDto>>,GetAllMessagesHandler>();
            services.AddScoped<IQueryHandler<GetMessageById, MessageViewDto>,GetMessageByIdHandler>();
            //диспетчеры
            services.AddScoped<CommandDispatcher>();
            services.AddScoped<QueryDispatcher>();
        }
    }
}