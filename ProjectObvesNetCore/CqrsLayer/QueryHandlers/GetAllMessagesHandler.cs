using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CqrsLayer.Interfaces;
using CqrsLayer.Queries;
using DataLayer.Entities;
using DataLayer.Repositories;
using PresentationLayer.Dto.Message;

namespace CqrsLayer.QueryHandlers
{
    public class GetAllMessagesHandler : IQueryHandler<GetAllMessages,List<MessageViewDto>>
    {
        private readonly MessageRepository _repository;
        private readonly IMapper _mapper;

        public GetAllMessagesHandler(MessageRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<List<MessageViewDto>> Execute(GetAllMessages query)
        {
            List<Message> messages = await _repository.GetAllAsync();
            return _mapper.Map<List<Message>, List<MessageViewDto>>(messages);
        }
    }
}