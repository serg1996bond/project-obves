using System.Threading.Tasks;
using AutoMapper;
using CqrsLayer.Interfaces;
using CqrsLayer.Queries;
using DataLayer.Entities;
using DataLayer.Repositories;
using PresentationLayer.Dto.Message;

namespace CqrsLayer.QueryHandlers
{
    public class GetMessageByIdHandler : IQueryHandler<GetMessageById, MessageViewDto>
    {
        private readonly MessageRepository _repository;
        private readonly IMapper _mapper;

        public GetMessageByIdHandler(MessageRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<MessageViewDto> Execute(GetMessageById query)
        {
            Message message = await _repository.FindByIdAsync(query.Id);
            return _mapper.Map<Message, MessageViewDto>(message);
        }
    }
}