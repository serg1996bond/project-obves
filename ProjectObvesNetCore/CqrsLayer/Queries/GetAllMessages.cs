using System.Collections.Generic;
using CqrsLayer.Interfaces;
using PresentationLayer.Dto.Message;

namespace CqrsLayer.Queries
{
    public class GetAllMessages : IQuery<List<MessageViewDto>>
    {
        
    }
}