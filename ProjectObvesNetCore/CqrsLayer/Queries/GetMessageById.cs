using CqrsLayer.Interfaces;
using PresentationLayer.Dto.Message;

namespace CqrsLayer.Queries
{
    public class GetMessageById : IQuery<MessageViewDto>
    {
        public int Id { get; set; }
    }
}