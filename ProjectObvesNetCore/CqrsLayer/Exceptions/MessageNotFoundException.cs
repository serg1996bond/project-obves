using System;

namespace CqrsLayer.Exceptions
{
    public class MessageNotFoundException : CqrsLayerException
    {
        public MessageNotFoundException(string message) : base(message)
        { }
    }
}