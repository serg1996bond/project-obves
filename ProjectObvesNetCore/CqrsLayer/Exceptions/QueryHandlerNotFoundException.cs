namespace CqrsLayer.Exceptions
{
    public class QueryHandlerNotFoundException : CqrsLayerException
    {
        public QueryHandlerNotFoundException(string message) : base(message) {}
    }
}