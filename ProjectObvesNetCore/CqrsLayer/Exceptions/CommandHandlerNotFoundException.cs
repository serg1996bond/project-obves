using System;

namespace CqrsLayer.Exceptions
{
    public class CommandHandlerNotFoundException : CqrsLayerException
    {
        public CommandHandlerNotFoundException(string message) : base(message)
        { }
    }
}