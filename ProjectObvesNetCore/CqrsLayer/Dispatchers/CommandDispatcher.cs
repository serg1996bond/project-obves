using System;
using System.Threading.Tasks;
using CqrsLayer.Exceptions;
using CqrsLayer.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CqrsLayer
{
    public class CommandDispatcher
    {
        private readonly IServiceProvider _provider;

        public CommandDispatcher(IServiceProvider provider)
        {
            _provider = provider;
        }
        public async Task Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (command == null) throw new ArgumentNullException("command");

            var handler = _provider.GetService<ICommandHandler<TCommand>>();
 
            if (handler == null) throw new CommandHandlerNotFoundException(typeof(TCommand).FullName);
 
            await handler.Execute(command);
        }
    }
}