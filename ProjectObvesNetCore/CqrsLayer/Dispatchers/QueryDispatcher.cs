using System;
using System.Threading.Tasks;
using CqrsLayer.Exceptions;
using CqrsLayer.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CqrsLayer.Dispatchers
{
    public class QueryDispatcher
    {
        private readonly IServiceProvider _provider;

        public QueryDispatcher(IServiceProvider provider)
        {
            _provider = provider;
        }
        public async Task<TResult> Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>
        {
            if (query == null) throw new ArgumentNullException("query");
 
            var handler = _provider.GetService<IQueryHandler<TQuery, TResult>>();
 
            if (handler == null) throw new QueryHandlerNotFoundException(typeof(TQuery).FullName);
 
            return await handler.Execute(query);
        }
    }
}