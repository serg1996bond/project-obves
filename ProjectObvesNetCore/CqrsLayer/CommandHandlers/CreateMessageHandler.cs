using System.Threading.Tasks;
using CqrsLayer.Commands;
using CqrsLayer.Interfaces;
using DataLayer.Entities;
using DataLayer.Repositories;

namespace CqrsLayer.CommandHandlers
{
    public class CreateMessageHandler : ICommandHandler<CreateMessage>
    {
        private readonly MessageRepository _repository;

        public CreateMessageHandler(MessageRepository repository)
        {
            _repository = repository;
        }

        public async Task Execute(CreateMessage command)
        {
            Message message = new Message()
            {
                Content = command.Content
            };
            await _repository.CreateAsync(message);
        }
    }
}