using System.Threading.Tasks;
using CqrsLayer.Commands;
using CqrsLayer.Exceptions;
using CqrsLayer.Interfaces;
using DataLayer.Entities;
using DataLayer.Repositories;

namespace CqrsLayer.CommandHandlers
{
    public class RemoveMessageHandler : ICommandHandler<RemoveMessage>
    {
        private readonly MessageRepository _repository;

        public RemoveMessageHandler(MessageRepository repository)
        {
            _repository = repository;
        }

        public async Task Execute(RemoveMessage command)
        {
            Message message = await _repository.FindByIdAsync(command.Id);
            if (message == null)
                throw new MessageNotFoundException("message with that id is not found");
            else
                await _repository.RemoveAsync(message);
        }
    }
}