using System.Threading.Tasks;
using AutoMapper;
using CqrsLayer.Commands;
using CqrsLayer.Interfaces;
using DataLayer.Entities;
using DataLayer.Repositories;
using PresentationLayer.Dto.Message;

namespace CqrsLayer.CommandHandlers
{
    public class UpdateMessageHandler : ICommandHandler<UpdateMessage>
    {
        private readonly MessageRepository _repository;
        private readonly IMapper _mapper;

        public UpdateMessageHandler(MessageRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task Execute(UpdateMessage command)
        {
            Message message = _mapper.Map<UpdateMessage, Message>(command);
            await _repository.UpdateAsync(message);
        }
    }
}