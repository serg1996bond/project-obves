using AutoMapper;
using CqrsLayer.Commands;
using DataLayer.Entities;

namespace CqrsLayer.AutoMapperProfiles
{
    public class MapperMessageCommandProfile : Profile
    {
        public MapperMessageCommandProfile()
        {
            CreateMap<UpdateMessage,Message>();
        }
    }
}