using System.Threading.Tasks;

namespace CqrsLayer.Interfaces
{
    public interface ICommandHandler<in TCommand>
    {
        public Task Execute(TCommand command);
    }
}