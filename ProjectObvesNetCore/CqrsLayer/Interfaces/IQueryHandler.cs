using System.Threading.Tasks;

namespace CqrsLayer.Interfaces
{
    public interface IQueryHandler<in TQuery, TResult> where TQuery : IQuery<TResult>
    {
        public Task<TResult> Execute(TQuery query);
    }
}