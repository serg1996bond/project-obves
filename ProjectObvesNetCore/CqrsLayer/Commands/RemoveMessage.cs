using CqrsLayer.Interfaces;

namespace CqrsLayer.Commands
{
    public class RemoveMessage : ICommand
    {
        public int Id { get; set; }
    }
}