using CqrsLayer.Interfaces;

namespace CqrsLayer.Commands
{
    public class UpdateMessage : ICommand
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}