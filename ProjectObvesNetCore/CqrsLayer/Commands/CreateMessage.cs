using CqrsLayer.Interfaces;

namespace CqrsLayer.Commands
{
    public class CreateMessage : ICommand
    {
        public string Content { get; set; }
    }
}