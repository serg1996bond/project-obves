namespace PresentationLayer.Dto.Message
{
    public class MessageViewDto
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}