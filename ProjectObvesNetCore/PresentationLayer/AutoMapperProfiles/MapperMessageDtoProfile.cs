using AutoMapper;
using DataLayer.Entities;
using PresentationLayer.Dto.Message;

namespace PresentationLayer.AutoMapperProfiles
{
    public class MapperMessageDtoProfile : Profile
    {
        public MapperMessageDtoProfile()
        {
            CreateMap<Message, MessageViewDto>();
            CreateMap<MessageViewDto, Message>();
        }
    }
}