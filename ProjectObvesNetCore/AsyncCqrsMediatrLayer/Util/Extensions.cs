using System.Collections.Generic;
using AsyncCqrsMediatrLayer.Behaviors;
using AsyncCqrsMediatrLayer.CommandHandlers;
using AsyncCqrsMediatrLayer.CommandHandlers.GenericHandlers;
using AsyncCqrsMediatrLayer.CommandHandlers.PersonHandlers;
using AsyncCqrsMediatrLayer.Commands;
using AsyncCqrsMediatrLayer.Commands.Person;
using AsyncCqrsMediatrLayer.Queries;
using AsyncCqrsMediatrLayer.Queries.Auth;
using AsyncCqrsMediatrLayer.Queries.Message;
using AsyncCqrsMediatrLayer.QueryHandlers;
using AsyncCqrsMediatrLayer.QueryHandlers.Auth;
using AsyncCqrsMediatrLayer.QueryHandlers.Generic;
using AsyncCqrsMediatrLayer.QueryHandlers.Message;
using AsyncCqrsMediatrLayer.RequestValidators;
using AsyncCqrsMediatrLayer.RequestValidators.Auth;
using AsyncCqrsMediatrLayer.RequestValidators.Message;
using AsyncCqrsMediatrLayer.RequestValidators.Person;
using DataLayer.Entities;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using PresentationLayer.Dto.Message;

namespace AsyncCqrsMediatrLayer.Util
{
    public static class Extensions
    {
        public static void AddAsyncCqrsMediatrLayer(this IServiceCollection services)
        {
            //обработчики комманд
                //Message
            services.AddScoped
            <
                IRequestHandler<CreateMessage, Unit>,
                GenericCreateTEntityHandler<Message,CreateMessage>
            >();
            services.AddScoped
            <
                IRequestHandler<RemoveMessage, Unit>,
                GenericRemoveTEntityHandler<Message,RemoveMessage>
            >();
            services.AddScoped<IRequestHandler
            <
                UpdateMessage, Unit>, 
                GenericUpdateTEntityHandler<Message,UpdateMessage>
            >();
                //Person
            services.AddScoped<IRequestHandler<CreatePerson,Unit>, CreatePersonHandler>();
            //обработчики запросов
                //Message
            services.AddScoped<IRequestHandler<GetAllMessages, List<MessageViewDto>>,GetAllMessagesHandler>();
            services.AddScoped
            <
                IRequestHandler<GetMessageById, MessageViewDto>,
                GenericGetTEntityByIdHandler<Message,GetMessageById,MessageViewDto>
            >();
                //Auth
            services.AddScoped<IRequestHandler<GetToken, OkObjectResult>, GetTokenHandler>();
            //валидаторы cqrs слоя
                //Message
            services.AddScoped<AbstractValidator<RemoveMessage>, RemoveMessageValidator>();
            services.AddScoped<AbstractValidator<CreateMessage>, CreateMessageValidator>();
            services.AddScoped<AbstractValidator<UpdateMessage>, UpdateMessageValidator>();
            services.AddScoped<AbstractValidator<GetMessageById>, GetMessageByIdValidator>();
            services.AddScoped<AbstractValidator<GetAllMessages>, GetAllMessagesValidator>();
                //Person
            services.AddScoped<AbstractValidator<CreatePerson>, CreatePersonValidator>();
                //Auth
            services.AddScoped<AbstractValidator<GetToken>, GetTokenValidator>();
            //доп слои пайплайна 
            //AHTUNG !!!! порядок выполнения слоев идентичен порядку регестрации в контейнере 
            services.AddScoped(typeof(IPipelineBehavior<,>),typeof(TestBehavior<,>));
            services.AddScoped(typeof(IPipelineBehavior<,>),typeof(LoggingBehavior<,>));
            services.AddScoped(typeof(IPipelineBehavior<,>),typeof(ValidatorBehavior<,>));
        }
    }
}