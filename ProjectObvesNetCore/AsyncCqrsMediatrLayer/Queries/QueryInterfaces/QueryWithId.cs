using MediatR;

namespace AsyncCqrsMediatrLayer.Queries.QueryInterfaces
{
    public interface IQueryWithId<TResult> : IRequest<TResult>
    {
        int GetId();
    }
}