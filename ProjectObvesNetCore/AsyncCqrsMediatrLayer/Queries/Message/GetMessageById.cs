using AsyncCqrsMediatrLayer.Queries.QueryInterfaces;
using PresentationLayer.Dto.Message;

namespace AsyncCqrsMediatrLayer.Queries.Message
{
    public class GetMessageById : IQueryWithId<MessageViewDto>
    {
        public int Id { get; set; }
        public int GetId()
        {
            return Id;
        }
    }
}