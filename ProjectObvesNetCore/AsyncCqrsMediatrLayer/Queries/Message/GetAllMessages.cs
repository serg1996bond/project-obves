using System.Collections.Generic;
using MediatR;
using PresentationLayer.Dto.Message;

namespace AsyncCqrsMediatrLayer.Queries.Message
{
    public class GetAllMessages : IRequest<List<MessageViewDto>>
    {
        
    }
}