using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AsyncCqrsMediatrLayer.Queries.Auth
{
    public class GetToken : IRequest<OkObjectResult>
    {
        public string Password { get; set; }
        public string Login { get; set; }
    }
}