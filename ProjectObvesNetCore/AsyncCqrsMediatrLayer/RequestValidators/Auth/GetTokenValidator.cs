using AsyncCqrsMediatrLayer.Queries.Auth;
using FluentValidation;

namespace AsyncCqrsMediatrLayer.RequestValidators.Auth
{
    public class GetTokenValidator : AbstractValidator<GetToken>
    {
        public GetTokenValidator()
        {
            RuleFor(x => x.Login).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}