using AsyncCqrsMediatrLayer.Commands.Person;
using FluentValidation;

namespace AsyncCqrsMediatrLayer.RequestValidators.Person
{
    public class CreatePersonValidator : AbstractValidator<CreatePerson>
    {
        public CreatePersonValidator()
        {
            RuleFor(x => x.Login).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
            //TODO придумать валидацию для роли
            RuleFor(x => x.Role).NotEmpty();
        }
    }
}