using AsyncCqrsMediatrLayer.Queries;
using AsyncCqrsMediatrLayer.Queries.Message;
using FluentValidation;

namespace AsyncCqrsMediatrLayer.RequestValidators.Message
{
    public class GetMessageByIdValidator : AbstractValidator<GetMessageById>
    {
        public GetMessageByIdValidator()
        {
            RuleFor(x => x.Id).GreaterThan(0);
        }
    }
}