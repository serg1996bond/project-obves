using AsyncCqrsMediatrLayer.Commands;
using FluentValidation;

namespace AsyncCqrsMediatrLayer.RequestValidators.Message
{
    public class CreateMessageValidator : AbstractValidator<CreateMessage>
    {
        public CreateMessageValidator()
        {
            RuleFor(x => x.Content).NotEmpty();
        }
    }
}