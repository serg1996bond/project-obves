using AsyncCqrsMediatrLayer.Commands;
using FluentValidation;

namespace AsyncCqrsMediatrLayer.RequestValidators.Message
{
    public class RemoveMessageValidator : AbstractValidator<RemoveMessage>
    {
        public RemoveMessageValidator()
        {
            RuleFor(x => x.Id).GreaterThan(0);
        }
    }
}