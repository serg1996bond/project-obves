using AsyncCqrsMediatrLayer.Commands;
using FluentValidation;

namespace AsyncCqrsMediatrLayer.RequestValidators.Message
{
    public class UpdateMessageValidator : AbstractValidator<UpdateMessage>
    {
        public UpdateMessageValidator()
        {
            RuleFor(x => x.Id).GreaterThan(0);
            RuleFor(x => x.Content).NotEmpty();
        }
    }
}