using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Queries.Message;
using AutoMapper;
using DataLayer.Repositories;
using MediatR;
using PresentationLayer.Dto.Message;

namespace AsyncCqrsMediatrLayer.QueryHandlers.Message
{
    public class GetAllMessagesHandler : IRequestHandler<GetAllMessages,List<MessageViewDto>>
    {
        private readonly MessageRepository _repository;
        private readonly IMapper _mapper;

        public GetAllMessagesHandler(MessageRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<List<MessageViewDto>> Handle(GetAllMessages request, CancellationToken cancellationToken)
        {
            List<DataLayer.Entities.Message> messages = await _repository.GetAllAsync();
            return _mapper.Map<List<DataLayer.Entities.Message>, List<MessageViewDto>>(messages);
        }
    }
}