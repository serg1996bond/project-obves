using System.Threading;
using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Queries.QueryInterfaces;
using AutoMapper;
using DataLayer.Repositories;
using MediatR;

namespace AsyncCqrsMediatrLayer.QueryHandlers.Generic
{
    public class GenericGetTEntityByIdHandler<TEntity,TRequest,TResult> : IRequestHandler<TRequest, TResult> 
        where TRequest : IQueryWithId<TResult> 
        where TEntity : class
    {
        private readonly BaseRepository<TEntity> _repository;
        private readonly IMapper _mapper;

        public GenericGetTEntityByIdHandler(BaseRepository<TEntity> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<TResult> Handle(TRequest request, CancellationToken cancellationToken)
        {
            TEntity entity = await _repository.FindByIdAsync(request.GetId());
            return _mapper.Map<TEntity, TResult>(entity);
        }
    }
}