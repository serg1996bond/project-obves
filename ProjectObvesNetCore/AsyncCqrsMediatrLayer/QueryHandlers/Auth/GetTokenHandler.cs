using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Queries.Auth;
using AuthLayer;
using AuthLayer.Exceptions;
using AuthLayer.Util;
using DataLayer.Entities;
using DataLayer.Repositories;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AsyncCqrsMediatrLayer.QueryHandlers.Auth
{
    public class GetTokenHandler : IRequestHandler<GetToken,OkObjectResult>
    {
        private readonly PersonRepository _repository;

        public GetTokenHandler(PersonRepository repository)
        {
            _repository = repository;
        }

        public async Task<OkObjectResult> Handle(GetToken request, CancellationToken cancellationToken)
        {
            var identity = await GetIdentity(request.Login, request.Password);
            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
 
            var response = new
            {
                access_token = encodedJwt,
                login = identity.Name
            };
            
            return new OkObjectResult(response);
        }
        
        private async Task<ClaimsIdentity> GetIdentity(string login, string password)
        {
            Person person = await _repository.FindByLogin(login);
            if (person == null)
            {
                throw new WrongLoginException();
            }
            if (!PasswordHashing.VerifyHashedPassword(person.Password,password))
            {
                throw new WrongPasswordException();
            }
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role)
            };
            ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}