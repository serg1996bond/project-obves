using System.Threading;
using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Commands.Person;
using AuthLayer.Util;
using AutoMapper;
using DataLayer.Entities;
using DataLayer.Repositories;
using MediatR;

namespace AsyncCqrsMediatrLayer.CommandHandlers.PersonHandlers
{
    public class CreatePersonHandler : IRequestHandler<CreatePerson>
    {
        private readonly PersonRepository _repository;
        private readonly IMapper _mapper;

        public CreatePersonHandler(PersonRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreatePerson request, CancellationToken cancellationToken)
        {
            request.Password = PasswordHashing.HashPassword(request.Password);
            Person person = _mapper.Map<CreatePerson, Person>(request);
            await _repository.CreateAsync(person);
            return Unit.Value;
        }
    }
}