using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataLayer.Repositories;
using MediatR;

namespace AsyncCqrsMediatrLayer.CommandHandlers.GenericHandlers
{
    public class GenericUpdateTEntityHandler<TEntity,TRequest> : IRequestHandler<TRequest>
        where TRequest : IRequest
        where TEntity : class
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<TEntity> _repository;

        public GenericUpdateTEntityHandler(IMapper mapper, BaseRepository<TEntity> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<Unit> Handle(TRequest request, CancellationToken cancellationToken)
        {
            TEntity entity = _mapper.Map<TRequest, TEntity>(request);
            await _repository.UpdateAsync(entity);
            return Unit.Value;
        }
    }
}