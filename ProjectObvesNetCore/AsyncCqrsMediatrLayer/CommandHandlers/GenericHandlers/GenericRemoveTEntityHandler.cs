using System.Threading;
using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Commands.CommandInterfaces;
using AsyncCqrsMediatrLayer.Exceptions;
using AutoMapper;
using DataLayer.Repositories;
using MediatR;

namespace AsyncCqrsMediatrLayer.CommandHandlers.GenericHandlers
{
    public class GenericRemoveTEntityHandler<TEntity,TRequest> : IRequestHandler<TRequest>
        where TRequest : ICommandWithId
        where TEntity : class
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<TEntity> _repository;

        public GenericRemoveTEntityHandler(IMapper mapper, BaseRepository<TEntity> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<Unit> Handle(TRequest request, CancellationToken cancellationToken)
        {
            TEntity entity = await _repository.FindByIdAsync(request.GetId());
            if (entity == null)
                throw new EntityNotFoundException(typeof(TEntity).Name + " with that id is not found");
            else
                await _repository.RemoveAsync(entity);
            return Unit.Value;
        }
    }
}