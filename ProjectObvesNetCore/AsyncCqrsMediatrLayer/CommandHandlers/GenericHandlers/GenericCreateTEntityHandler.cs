using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataLayer.Repositories;
using MediatR;

namespace AsyncCqrsMediatrLayer.CommandHandlers.GenericHandlers
{
    public class GenericCreateTEntityHandler<TEntity,TRequest> : IRequestHandler<TRequest> 
        where TRequest : IRequest<Unit> 
        where TEntity : class
    {
        private readonly IMapper _mapper;
        private readonly BaseRepository<TEntity> _repository;

        public GenericCreateTEntityHandler(IMapper mapper, BaseRepository<TEntity> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<Unit> Handle(TRequest request, CancellationToken cancellationToken)
        {
            TEntity entity = _mapper.Map<TRequest, TEntity>(request);
            await _repository.CreateAsync(entity);
            return Unit.Value;
        }
    }
}