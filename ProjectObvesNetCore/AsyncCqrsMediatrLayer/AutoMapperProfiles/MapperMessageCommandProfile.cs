using AsyncCqrsMediatrLayer.Commands;
using AsyncCqrsMediatrLayer.Commands.Person;
using AsyncCqrsMediatrLayer.Queries.Auth;
using AutoMapper;
using DataLayer.Entities;

namespace AsyncCqrsMediatrLayer.AutoMapperProfiles
{
    public class MapperMessageCommandProfile : Profile
    {
        public MapperMessageCommandProfile()
        {
            //Message
            CreateMap<UpdateMessage,Message>();
            CreateMap<CreateMessage, Message>();
            //Person
            CreateMap<CreatePerson, Person>();
        }
    }
}