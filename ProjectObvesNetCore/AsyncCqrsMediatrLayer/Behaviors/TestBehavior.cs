using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace AsyncCqrsMediatrLayer.Behaviors
{
    public class TestBehavior<TRequest, TResponse>
        : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<TestBehavior<TRequest, TResponse>> _logger;
        public TestBehavior(ILogger<TestBehavior<TRequest, TResponse>> logger) =>
            _logger = logger;

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _logger.LogInformation($"Test Handling {typeof(TRequest).Name}");
            var response = await next();
            _logger.LogInformation($"Test Handled {typeof(TResponse).Name}");
            return response;
        }
    }

}