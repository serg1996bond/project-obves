using System.Threading;
using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Commands;
using AsyncCqrsMediatrLayer.Exceptions;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Microsoft.Extensions.Logging;

namespace AsyncCqrsMediatrLayer.Behaviors
{
    public class ValidatorBehavior<TRequest, TResponse>
        : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly AbstractValidator<TRequest> _validator;
        private readonly ILogger<TestBehavior<TRequest, TResponse>> _logger;

        public ValidatorBehavior(AbstractValidator<TRequest> validator, ILogger<TestBehavior<TRequest, TResponse>> logger)
        {
            _validator = validator;
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            ValidationResult results = await _validator.ValidateAsync(request, cancellationToken);
            if (results.Errors.Count>0)
            {
                _logger.LogWarning("Validation has failed");
                throw new ValidateCommandException(results.Errors[0].ToString());
            }
            var response = await next();
            return response;
        }
    }
}