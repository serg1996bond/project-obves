using MediatR;

namespace AsyncCqrsMediatrLayer.Commands.CommandInterfaces
{
    public interface ICommandWithId : IRequest
    {
        int GetId();
    }
}