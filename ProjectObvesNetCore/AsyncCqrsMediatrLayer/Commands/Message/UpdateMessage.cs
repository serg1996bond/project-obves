using MediatR;

namespace AsyncCqrsMediatrLayer.Commands
{
    public class UpdateMessage : IRequest
    {
        public int Id { get; set; }
        public string Content { get; set; }
    }
}