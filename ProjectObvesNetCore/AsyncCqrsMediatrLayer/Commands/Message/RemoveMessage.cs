using AsyncCqrsMediatrLayer.Commands.CommandInterfaces;
using MediatR;

namespace AsyncCqrsMediatrLayer.Commands
{
    public class RemoveMessage : ICommandWithId
    {
        public int Id { get; set; }
        public int GetId()
        {
            return Id;
        }
    }
}