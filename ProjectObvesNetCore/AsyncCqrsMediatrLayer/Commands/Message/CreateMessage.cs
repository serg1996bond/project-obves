using MediatR;

namespace AsyncCqrsMediatrLayer.Commands
{
    public class CreateMessage : IRequest<Unit>
    {
        public string Content { get; set; }
    }
}