using MediatR;

namespace AsyncCqrsMediatrLayer.Commands.Person
{
    public class CreatePerson : IRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}