namespace AsyncCqrsMediatrLayer.Exceptions
{
    public class EntityNotFoundException : CqrsLayerException
    {
        public EntityNotFoundException(string message) : base(message)
        { }
    }
}