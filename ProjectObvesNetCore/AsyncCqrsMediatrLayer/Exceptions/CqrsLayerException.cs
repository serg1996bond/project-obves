using System;

namespace AsyncCqrsMediatrLayer.Exceptions
{
    public class CqrsLayerException : Exception
    {
        public CqrsLayerException(){}
        public CqrsLayerException(string message) : base(message){}
    }
}