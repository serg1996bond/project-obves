namespace AsyncCqrsMediatrLayer.Exceptions
{
    public class ValidateCommandException : CqrsLayerException
    {
        public ValidateCommandException()
        {
        }
        
        public ValidateCommandException(string message) : base(message){}
    }
}