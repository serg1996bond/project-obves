using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace AuthLayer
{
    public class AuthOptions
    {
        public const string ISSUER = "MyAuthServer"; // издатель токена
        public const string AUDIENCE = "MyAuthClient"; // потребитель токена
        const string KEY = "UAHF*zcSon0?M,A#zqJ>}z'zWH93*+^_vrsZ1g0/UAL_HtW1BX.-R;OlIO_76u5";   // ключ для шифрации
        public const int LIFETIME = 10; // время жизни токена - 10 минут
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}