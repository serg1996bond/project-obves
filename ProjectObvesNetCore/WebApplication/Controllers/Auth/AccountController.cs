using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Queries.Auth;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers.Auth
{
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        
        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("/token")]
        public async Task<IActionResult> Token([FromBody]GetToken command)
        {
            return await _mediator.Send(command);
        }
 

    }
}