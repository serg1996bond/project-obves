using System.Collections.Generic;
using System.Threading.Tasks;
using CqrsLayer;
using CqrsLayer.Commands;
using CqrsLayer.Dispatchers;
using CqrsLayer.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Dto.Message;

namespace WebApplication.Controllers.AsyncCqrs
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly CommandDispatcher _commandDispatcher;
        private readonly QueryDispatcher _queryDispatcher;

        public MessageController(CommandDispatcher commandDispatcher, QueryDispatcher queryDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        [HttpPut] 
        public async Task<Unit> CreateMessage ([FromBody] CreateMessage command)
        {
            await _commandDispatcher.Execute(command);
            return Unit.Value;
        }

        [HttpDelete]
        public async Task RemoveMessage([FromBody] RemoveMessage command)
        {
            await _commandDispatcher.Execute(command);
        }

        [HttpGet]
        public async Task<List<MessageViewDto>> GetAllMessages()
        {
            return await _queryDispatcher.Execute<GetAllMessages,List<MessageViewDto>>(new GetAllMessages());
        }

        [HttpGet("{id}")]
        public async Task<MessageViewDto> GetMessageById(int id)
        {
            return await _queryDispatcher.Execute<GetMessageById,MessageViewDto>(new GetMessageById() {Id = id});
        }

        [HttpPost]
        public async Task UpdateMessage([FromBody] UpdateMessage command)
        {
            await _commandDispatcher.Execute(command);
        }
    }
}