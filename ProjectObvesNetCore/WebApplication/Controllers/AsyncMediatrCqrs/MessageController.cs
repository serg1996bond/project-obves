using System.Collections.Generic;
using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Commands;
using AsyncCqrsMediatrLayer.Queries;
using AsyncCqrsMediatrLayer.Queries.Message;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PresentationLayer.Dto.Message;

namespace WebApplication.Controllers.AsyncMediatrCqrs
{
    [ApiController]
    [Route("mediatr/[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MessageController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPut] 
        public async Task<StatusCodeResult> CreateMessage ([FromBody] CreateMessage command)
        {
            await _mediator.Send(command);
            return StatusCode(201);
        }

        [HttpDelete]
        public async Task RemoveMessage([FromBody] RemoveMessage command)
        {
            await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<List<MessageViewDto>> GetAllMessages()
        {
            return await _mediator.Send(new GetAllMessages());
        }

        [HttpGet("{id}")]
        public async Task<MessageViewDto> GetMessageById(int id)
        {
            return await _mediator.Send(new GetMessageById() {Id = id});
        }

        [HttpPost]
        public async Task UpdateMessage([FromBody] UpdateMessage command)
        {
            await _mediator.Send(command);
        }
    }
}