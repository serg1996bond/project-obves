using System.Threading.Tasks;
using AsyncCqrsMediatrLayer.Commands.Person;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers.AsyncMediatrCqrs
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PersonController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPut]
        public async Task<IActionResult> CreatePerson([FromBody] CreatePerson request)
        {
            await _mediator.Send(request);
            return StatusCode(201);
        }
    }
}