using AsyncCqrsMediatrLayer.Util;
using AuthLayer.Util;
using AutoMapper;
using CqrsLayer.AutoMapperProfiles;
using CqrsLayer.Util;
using DataLayer.Util;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PresentationLayer.AutoMapperProfiles;

namespace WebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // контроллеры
            services.AddControllers();
            //CQRS
            services.AddCqrsLayer();
            //CQRS + MediatR
            services.AddAsyncCqrsMediatrLayer();
            //Data
            services.AddDataLayer();
            //автомаппер
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MapperMessageDtoProfile());
                mc.AddProfile(new MapperMessageCommandProfile());
                mc.AddProfile(new AsyncCqrsMediatrLayer.AutoMapperProfiles.MapperMessageCommandProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            //MediatR
            services.AddMediatR(typeof(Startup));
            //Auth
            services.AddAuthLayer();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}